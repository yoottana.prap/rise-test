class Calculator {
    carts = []
    products = [
        { name: "Red set", price: 50 },
        { name: "Green set", price: 40 },
        { name: "Blue set", price: 30 },
        { name: "Yellow set", price: 50 },
        { name: "Pink set", price: 80 },
        { name: "Purple set", price: 90 },
        { name: "Orange set", price: 120 },
    ]
    constructor() { }

    CalculatorProduct(isHaveMemberCard,) {
        const DISCOUNT = {
            FIVEPERCENTAGE: 5,
            TENPERCENTAGE: 10
        }
        const getCart = this.getShoppingCart()
        const totalPrice = this.grandTotal(getCart)
        const findItemForDiscount = this.carts.find(item => item.product.name === "Orange set" || item.product.name === "Pink set" || item.product.name === "Green set" && item.qty >= 2)
        if (findItemForDiscount && isHaveMemberCard) {
            let discount = DISCOUNT.FIVEPERCENTAGE + DISCOUNT.TENPERCENTAGE
            return {
                allProduct: getCart,
                totalPrice: totalPrice - ((discount / totalPrice) * totalPrice),
            }
        }
        else if(findItemForDiscount) {
            let discount = DISCOUNT.FIVEPERCENTAGE
            return {
                allProduct: getCart,
                totalPrice: totalPrice - ((discount / totalPrice) * totalPrice),
            }
        }
        else if (isHaveMemberCard) {
            let discount = DISCOUNT.TENPERCENTAGE
            return {
                allProduct: getCart,
                totalPrice: totalPrice - ((discount / totalPrice) * totalPrice),
            } 
        }
        else {
            return {
                allProduct: getCart,
                totolPrice: totalPrice
            }
        }
    }

    addCart(name) {
        // find one item 
        const findItemInAllProducts = this.products.find((item => item.name === name))
        const findDuplicateItem = this.carts.findIndex(item => item.product.name === name)
        if (findItemInAllProducts && findDuplicateItem == -1) {
            this.carts.push({
                product: findItemInAllProducts,
                qty: 1,
                totalPrice: findItemInAllProducts.price
            })
        } else {
            this.carts[findDuplicateItem].qty = this.carts[findDuplicateItem].qty + 1
            this.carts[findDuplicateItem].totalPrice = this.carts[findDuplicateItem].product.price
            this.carts[findDuplicateItem].totalPrice = this.carts[findDuplicateItem].totalPrice * this.carts[findDuplicateItem].qty
        }
    }

    getShoppingCart() {
        return this.carts
    }

    grandTotal(cart) {
        return cart.reduce((sum, i) => {
            return sum + (i.totalPrice)
        }, 0)
    };

}


function buyRedAndGreen() {
    console.log("buy don't have discount");
    let c = new Calculator()
    c.addCart("Red set")
    c.addCart("Green set")
    console.log(c.CalculatorProduct(false));
}

function buyRedAndGreenTenPercentage() {
    console.log("buy have discount 10%");
    let c2 = new Calculator()
    c2.addCart("Red set")
    c2.addCart("Green set")
    console.log(c2.CalculatorProduct(true));
}

function buyProductConditionFivePercentage() {
    console.log("buy have two item condition discount 5%");
    let c3 = new Calculator()
    c3.addCart("Red set")
    c3.addCart("Green set")
    c3.addCart("Green set")
    console.log(c3.CalculatorProduct(false));
}

function buyProductConditionFivePercentageAndHaveMemberCardTenPercentage() {
    console.log("buy have two item condition discount 5%   member is 10 %");
    let c3 = new Calculator()
    c3.addCart("Red set")
    c3.addCart("Green set")
    c3.addCart("Green set")
    console.log(c3.CalculatorProduct(true));
}


buyRedAndGreen()
buyRedAndGreenTenPercentage()
buyProductConditionFivePercentage()
buyProductConditionFivePercentageAndHaveMemberCardTenPercentage()